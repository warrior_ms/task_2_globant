# Reading the content of file
with open('text_task.txt', 'r') as f:
    data = f.readlines()

columns = data[0].split(';')
columns[-1] = columns[-1][: -1]

# Preparing nested dictionary object of the data
data = data[1:]
records = dict()
for x, i in enumerate(data):
    i = i[: -1]
    fields = i.split(';')
    tmp = dict(zip(columns, fields))

    records[x] = tmp

# Nested dictionary with no duplicates
fixed_data = {}

#list to hold salaries of employees, which can then be used for finding minimum salary
salaries = []

# Creating new dictionary with no duplicates
for key,value in records.items():
    if value not in fixed_data.values():
        fixed_data[key] = value
        salaries.append(int(value['salary']))

min_sal = min(salaries)

# Printing minimum salary
print(min_sal)

# Filtering employees with lowest salaries using dict comprehensions
result_dict = { key: value for key, value in fixed_data.items() if value['salary'] == str(min_sal) }

# Printing the records that meet the criteria
for key, value in result_dict.items():
    print(value)